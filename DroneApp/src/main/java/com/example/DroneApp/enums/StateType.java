package com.example.DroneApp.enums;

public enum StateType {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
