package com.example.DroneApp.ApiHelper;

import com.example.DroneApp.dto.MedicationReq;
import com.example.DroneApp.entity.Medication;
import com.example.DroneApp.services.DroneService;
import com.example.DroneApp.services.MedicationService;
import com.example.DroneApp.vo.MedicationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Component
public class MedicationApiImpl {
    @Autowired
    private MedicationService medicationService;
    @Autowired
    private DroneService droneService;

    public void create(String name, Double weight, String code, Long droneId, MultipartFile file) {
        Medication medication=MedicationReq.toMedication(name, weight, code, droneId, file);
        medication.setDrone(droneService.read(droneId));
        medicationService.create(medication);
    }

    public List<MedicationVo> readByDroneId(Long droneId) {

        List<Medication> medicationList = medicationService.readByDrone(droneId);
        List<MedicationVo> medicationVoList=new ArrayList<>();
        for(Medication medication:medicationList)
            medicationVoList.add(new MedicationVo(medication));
        return medicationVoList;

    }
}
