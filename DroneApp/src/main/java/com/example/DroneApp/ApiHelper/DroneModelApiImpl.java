package com.example.DroneApp.ApiHelper;

import com.example.DroneApp.entity.DroneModel;
import com.example.DroneApp.services.DroneModelService;
import com.example.DroneApp.vo.DroneModelVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DroneModelApiImpl {
    @Autowired
    private DroneModelService droneModelService;
    public List<DroneModelVo> readAll() {
        List<DroneModelVo> droneModelVoList = new ArrayList<>();
        List<DroneModel> droneModelList = droneModelService.readAll();
        for(DroneModel droneModel:droneModelList){
            droneModelVoList.add(new DroneModelVo(droneModel));
        }
        return droneModelVoList;
    }
}
