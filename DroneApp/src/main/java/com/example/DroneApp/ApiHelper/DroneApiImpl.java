package com.example.DroneApp.ApiHelper;

import com.example.DroneApp.dto.DroneDto;
import com.example.DroneApp.dto.DroneReq;
import com.example.DroneApp.entity.Drone;
import com.example.DroneApp.entity.DroneModel;
import com.example.DroneApp.enums.StateType;
import com.example.DroneApp.exception.DroneModelNotFoundException;
import com.example.DroneApp.repository.DroneModelRepository;
import com.example.DroneApp.services.DroneService;
import com.example.DroneApp.vo.DroneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class DroneApiImpl {

    @Autowired
    private DroneService droneService;
    @Autowired
    private DroneModelRepository droneModelRepository;
//    @Autowired
//    private ModelMap modelMap;

    public void create(DroneReq droneReq){
        Drone drone = DroneReq.toDrone(droneReq);

        Optional<DroneModel> droneModelOptional = droneModelRepository.findById(droneReq.getModelId());

        drone.setModel(droneModelRepository.findById(droneReq.getModelId()).get());
        if(drone.getBatteryCapacity()<25){
            drone.setState(StateType.IDLE);
        }
        droneService.create(drone);
    }

    public List<DroneVo> readByStatus(StateType state) {
        List<DroneVo> droneVoList = new ArrayList<>();
        List<Drone> droneList = droneService.readByState(state);
        for(Drone drone:droneList)
            droneVoList.add(new DroneVo(drone));
        return droneVoList;
    }

    public DroneVo readById(Long id) {

        Drone drone = droneService.read(id);
        return new DroneVo(drone);

    }
}
