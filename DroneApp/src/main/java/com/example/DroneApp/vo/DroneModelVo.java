package com.example.DroneApp.vo;

import com.example.DroneApp.entity.DroneModel;
import lombok.Data;

import javax.persistence.Column;
@Data
public class DroneModelVo {

    private Long id;

    private String name;


    public DroneModelVo(DroneModel droneModel) {
        this.setId(droneModel.getId());
        this.setName(droneModel.getName());
    }
}
