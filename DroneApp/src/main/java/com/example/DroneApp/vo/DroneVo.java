package com.example.DroneApp.vo;

import com.example.DroneApp.entity.Drone;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DroneVo {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("serial_number")
    private String serialNumber;

    @JsonProperty("model")
    private String model;

    @JsonProperty("weight_limit")
    private Double weightLimit;

    @JsonProperty("battery_capacity")
    private Double batteryCapacity;

    @JsonProperty("state")
    private String state;

    public DroneVo(Drone drone) {
        id = drone.getId();
        serialNumber=drone.getSerialNumber();
        model=drone.getModel().getName();
        weightLimit=drone.getWeightLimit();
        batteryCapacity=drone.getBatteryCapacity();
        state=drone.getState().toString();
    }
}
