package com.example.DroneApp.vo;

import com.example.DroneApp.entity.Medication;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.File;

@Data
public class MedicationVo {

    private Long id;

    private String name;

    private Double weight;

    private String code;

    private byte[] image;

    @JsonProperty("drone")
    private String drone;

    public MedicationVo(Medication medication) {
        id = medication.getId();
        code = medication.getCode();
        weight = medication.getWeight();
        name = medication.getName();
        image = medication.getImage();
        drone = medication.getDrone().getModel().getName();
    }
}
