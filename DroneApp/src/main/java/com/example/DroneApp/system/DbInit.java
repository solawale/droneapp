package com.example.DroneApp.system;


import com.example.DroneApp.entity.DroneModel;
import com.example.DroneApp.repository.DroneModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DbInit {

    @Autowired
    private DroneModelRepository droneModelRepository;

    @PostConstruct
    private void initialise(){
        initModel();
    }

    private void initModel(){
        DroneModel droneModel=new DroneModel();
        droneModel.setName("Lightweight");
        droneModelRepository.save(droneModel);
        droneModel=new DroneModel();
        droneModel.setName("Middleweight");
        droneModelRepository.save(droneModel);
        droneModel=new DroneModel();
        droneModel.setName("Cruiserweight");
        droneModelRepository.save(droneModel);
        droneModel=new DroneModel();
        droneModel.setName("Heavyweight");
        droneModelRepository.save(droneModel);
    }


}
