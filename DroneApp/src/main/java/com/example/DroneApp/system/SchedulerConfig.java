package com.example.DroneApp.system;

import com.example.DroneApp.entity.Drone;
import com.example.DroneApp.enums.StateType;
import com.example.DroneApp.repository.DroneRepository;
import com.example.DroneApp.services.MedicationServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

@Configuration
@EnableScheduling
public class SchedulerConfig {

    @Autowired
    private DroneRepository droneRepository;

    private final Logger logger = LoggerFactory.getLogger(MedicationServiceImpl.class);

    @Scheduled(fixedDelay = 1000)
    public void checkDroneBattery(){

        logger.info("=======Drone Battery Level Checking=======");
        List<Drone> droneList = droneRepository.findAll();

        for(Drone drone:droneList){
            if(drone.getBatteryCapacity()>=25)
                logger.info("Drone model %s, with serial number %s has battery level %f", drone.getModel().getName(), drone.getSerialNumber(), drone.getBatteryCapacity());
            else {
                logger.warn("Drone model %s, with serial number %s has battery level %f", drone.getModel().getName(), drone.getSerialNumber(), drone.getBatteryCapacity());
                if(drone.getState()== StateType.LOADING){
                    drone.setState(StateType.IDLE);
                }
                droneRepository.save(drone);
            }

        }

    }

}
