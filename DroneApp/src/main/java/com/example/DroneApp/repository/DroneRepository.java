package com.example.DroneApp.repository;

import com.example.DroneApp.entity.Drone;
import com.example.DroneApp.enums.StateType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.sql.Timestamp;
import java.util.List;

public interface DroneRepository extends JpaRepository<Drone, Long> {
    List<Drone> findAllByState(StateType state);



}
