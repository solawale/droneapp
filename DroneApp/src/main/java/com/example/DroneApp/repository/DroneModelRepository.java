package com.example.DroneApp.repository;

import com.example.DroneApp.entity.DroneModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DroneModelRepository extends JpaRepository<DroneModel, Long> {
}
