package com.example.DroneApp.repository;

import com.example.DroneApp.entity.Drone;
import com.example.DroneApp.entity.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.sql.Timestamp;
import java.util.List;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
    List<Medication> findAllByDrone(Drone drone);

    @Query("SELECT sum(m.weight) from Medication m WHERE m.drone = ?1")
    Double findTotalWeightByDrone(Drone drone);

}
