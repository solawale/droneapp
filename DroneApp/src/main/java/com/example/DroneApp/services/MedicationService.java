package com.example.DroneApp.services;

import com.example.DroneApp.entity.Drone;
import com.example.DroneApp.entity.Medication;

import java.util.List;

public interface MedicationService {
    public void create(Medication medication);
    public Medication read(Long id);
    public Medication update(Medication medication);
    public void delete(Long id);

    List<Medication> readByDrone(Long droneId);
}
