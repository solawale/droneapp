package com.example.DroneApp.services;

import com.example.DroneApp.entity.Drone;
import com.example.DroneApp.entity.DroneModel;

import java.util.List;

public interface DroneModelService {

    public List<DroneModel> readAll();

}
