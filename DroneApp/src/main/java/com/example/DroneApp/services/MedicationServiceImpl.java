package com.example.DroneApp.services;

import com.example.DroneApp.entity.Drone;
import com.example.DroneApp.entity.Medication;
import com.example.DroneApp.exception.RecordNotFoundException;
import com.example.DroneApp.exception.WeightLimitException;
import com.example.DroneApp.repository.DroneRepository;
import com.example.DroneApp.repository.MedicationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MedicationServiceImpl implements MedicationService{
    @Autowired
    private MedicationRepository medicationRepository;
    @Autowired
    private DroneRepository droneRepository;

    private final Logger logger = LoggerFactory.getLogger(MedicationServiceImpl.class);

    @Override
    public void create(Medication medication) {

        Optional<Drone> droneOptional = Optional.ofNullable(droneRepository.findById(medication.getDrone().getId()).orElseThrow(() -> new RecordNotFoundException()));

//        Drone drone = droneRepository.findById(medication.getDrone().getId()).get();
        Drone drone = droneOptional.get();

        Double totalWeight = medicationRepository.findTotalWeightByDrone(drone)==null?0:medicationRepository.findTotalWeightByDrone(drone);
        Double total = totalWeight + medication.getWeight();
        if(drone.getWeightLimit() < total){
            throw new WeightLimitException();
        }
        medicationRepository.save(medication);

        logger.info(String.format("Medication %s for drone serial number %s added", medication.getName(), drone.getSerialNumber()));
    }

    @Override
    public Medication read(Long id) {
        return medicationRepository.findById(id).get();
    }

    @Override
    public Medication update(Medication medication) {
        return medicationRepository.save(medication);
    }

    @Override
    public void delete(Long id) {
        medicationRepository.delete(medicationRepository.findById(id).get());
    }

    @Override
    public List<Medication> readByDrone(Long droneId) {
        Optional<Drone> droneOptional = Optional.ofNullable(droneRepository.findById(droneId).orElseThrow(() -> new RecordNotFoundException()));
        return medicationRepository.findAllByDrone(droneOptional.get());
    }
}
