package com.example.DroneApp.services;

import com.example.DroneApp.entity.Drone;
import com.example.DroneApp.enums.StateType;
import com.example.DroneApp.repository.DroneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DroneService {

    public void create(Drone drone);
    public Drone read(Long id);
    public Drone update(Drone drone);
    public void delete(Long id);


    List<Drone> readByState(StateType state);
}
