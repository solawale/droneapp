package com.example.DroneApp.services;

import com.example.DroneApp.entity.Drone;
import com.example.DroneApp.enums.StateType;
import com.example.DroneApp.exception.RecordNotFoundException;
import com.example.DroneApp.repository.DroneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class DroneServiceImp implements DroneService{

    @Autowired
    private DroneRepository droneRepository;

    private final Logger logger = LoggerFactory.getLogger(DroneServiceImp.class);

    public static Map<String, StateType> droneStateMap = new HashMap<>();
    @PostConstruct
    private void loadDroneState(){
        droneStateMap.put(StateType.IDLE.toString(), StateType.IDLE);
        droneStateMap.put(StateType.LOADING.toString(), StateType.LOADING);
        droneStateMap.put(StateType.LOADED.toString(), StateType.LOADED);
        droneStateMap.put(StateType.DELIVERED.toString(), StateType.DELIVERED);
        droneStateMap.put(StateType.DELIVERING.toString(), StateType.DELIVERING);
        droneStateMap.put(StateType.RETURNING.toString(), StateType.RETURNING);
    }

    @Override
    public void create(Drone drone) {
        droneRepository.save(drone);
        logger.info(String.format("Drone model %s woth serial number %s added", drone.getModel().getName(), drone.getSerialNumber()));
    }

    @Override
    public Drone read(Long id) {
        Optional<Drone> droneOptional= Optional.ofNullable(droneRepository.findById(id).orElseThrow(() -> new RecordNotFoundException()));
        if(droneOptional.isPresent()){
            return droneOptional.get();
        }
        return null;
    }

    @Override
    public Drone update(Drone drone) {
        return droneRepository.save(drone);
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public List<Drone> readByState(StateType state) {
        return droneRepository.findAllByState(state);
    }
}
