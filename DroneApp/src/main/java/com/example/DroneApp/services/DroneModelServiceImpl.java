package com.example.DroneApp.services;

import com.example.DroneApp.entity.DroneModel;
import com.example.DroneApp.repository.DroneModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DroneModelServiceImpl implements DroneModelService{
    @Autowired
    private DroneModelRepository droneModelRepository;
    @Override
    public List<DroneModel> readAll() {
        return droneModelRepository.findAll();
    }
}
