package com.example.DroneApp.entity;

import com.example.DroneApp.enums.StateType;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "drone")
public class Drone {


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "serial_number")
    private String serialNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "model")
    private DroneModel model;

    @Column(name = "weight_limit")
    private Double weightLimit;

    @Column(name = "battery_capacity")
    private Double batteryCapacity;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private StateType state;

    @OneToMany(mappedBy = "drone", fetch = FetchType.LAZY)
    private Set<Medication> medications;

}
