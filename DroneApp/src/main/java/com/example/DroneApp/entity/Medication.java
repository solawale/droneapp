package com.example.DroneApp.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.File;

@Data
@Entity
@Table(name = "medication")
public class Medication {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "weight")
    private Double weight;

    @Column(name = "code")
    private String code;

    @Column(name = "image")
    @Lob
    private byte[] image;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "drone")
    private Drone drone;

}
