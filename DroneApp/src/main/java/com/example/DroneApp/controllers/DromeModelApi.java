package com.example.DroneApp.controllers;

import com.example.DroneApp.ApiHelper.DroneApiImpl;
import com.example.DroneApp.ApiHelper.DroneModelApiImpl;
import com.example.DroneApp.dto.DroneReq;
import com.example.DroneApp.enums.StateType;
import com.example.DroneApp.vo.DroneModelVo;
import com.example.DroneApp.vo.DroneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class DromeModelApi {

    @Autowired
    private DroneModelApiImpl droneModelApiImpl;

    @GetMapping(value = {"/api/drone_model"})
    public ResponseEntity<?> getAllDroneModel(HttpServletRequest request){
        List<DroneModelVo> droneModelVoList = droneModelApiImpl.readAll();
        return new ResponseEntity<>(droneModelVoList, HttpStatus.OK);
    }



}
