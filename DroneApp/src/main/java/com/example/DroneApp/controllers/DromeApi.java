package com.example.DroneApp.controllers;

import com.example.DroneApp.ApiHelper.DroneApiImpl;
import com.example.DroneApp.dto.DroneReq;
import com.example.DroneApp.enums.StateType;
import com.example.DroneApp.vo.DroneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class DromeApi {

    @Autowired
    private DroneApiImpl droneApiImpl;

    @PostMapping(value = {"/api/drone"})
    public ResponseEntity<?> createDrone(@RequestBody(required = true)DroneReq droneReq, HttpServletRequest request){
        droneApiImpl.create(droneReq);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @GetMapping(value = {"/api/drone/available"})
    public ResponseEntity<?> getDroneAvailable(HttpServletRequest request){
        List<DroneVo> droneVoList = droneApiImpl.readByStatus(StateType.IDLE);
        return new ResponseEntity<>(droneVoList, HttpStatus.OK);
    }

    @GetMapping(value = {"/api/drone/{id}/battery"})
    public ResponseEntity<?> getDroneBatteryLevel(@PathVariable(required = true) Long id, HttpServletRequest request){
        DroneVo droneVo = droneApiImpl.readById(id);
        return new ResponseEntity<>(droneVo, HttpStatus.OK);
    }

}
