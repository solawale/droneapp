package com.example.DroneApp.controllers;

import com.example.DroneApp.ApiHelper.MedicationApiImpl;
import com.example.DroneApp.dto.DroneReq;
import com.example.DroneApp.dto.MedicationReq;
import com.example.DroneApp.vo.MedicationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class MedicationApi {

    @Autowired
    private MedicationApiImpl medicationApiImpl;

    @PostMapping(value = {"/api/medication"})
//    public ResponseEntity<?> loadDroneMedication(@RequestBody(required = true) MedicationReq medicationReq, HttpServletRequest request){
    public ResponseEntity<?> loadDroneMedication(@RequestParam(name = "name", required = true) String name,
                                                 @RequestParam(name = "weight", required = true) Double weight,
                                                 @RequestParam(name = "code", required = true) String code,
                                                 @RequestParam(name = "drone_id", required = true) Long droneId,
                                                 @RequestParam(name = "image", required = true) MultipartFile file,
                                                 HttpServletRequest request){
        medicationApiImpl.create(name, weight, code, droneId, file);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @GetMapping(value = {"/api/medication"})
    public ResponseEntity<?> readDroneMedication(@RequestParam(required = true) Long droneId, HttpServletRequest request){
        List<MedicationVo> medicationVoList=medicationApiImpl.readByDroneId(droneId);
        return new ResponseEntity<>(medicationVoList, HttpStatus.OK);
    }
}
