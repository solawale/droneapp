package com.example.DroneApp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class WeightLimitException extends RuntimeException {

    @ExceptionHandler(value = WeightLimitException.class)
    public ResponseEntity<Object> exception(WeightLimitException e){
        return new ResponseEntity<>("Weight limit exceeded", HttpStatus.NOT_FOUND);
    }
}
