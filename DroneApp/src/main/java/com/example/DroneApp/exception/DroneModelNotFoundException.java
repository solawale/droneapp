package com.example.DroneApp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DroneModelNotFoundException extends RuntimeException{

    @ExceptionHandler(value = DroneModelNotFoundException.class)
    public ResponseEntity<Object> exception(DroneModelNotFoundException e){
        return new ResponseEntity<>("Drone model not found", HttpStatus.NOT_FOUND);
    }
}
