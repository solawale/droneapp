package com.example.DroneApp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class InvalidDroneStateException extends RuntimeException{

    @ExceptionHandler(value = InvalidDroneStateException.class)
    public ResponseEntity<Object> exception(InvalidDroneStateException e){
        return new ResponseEntity<>("Invalid Drone state", HttpStatus.NOT_FOUND);
    }
}
