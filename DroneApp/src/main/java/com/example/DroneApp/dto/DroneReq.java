package com.example.DroneApp.dto;

import com.example.DroneApp.entity.Drone;
import com.example.DroneApp.exception.InvalidDroneStateException;
import com.example.DroneApp.services.DroneServiceImp;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DroneReq {

    @JsonProperty("serial_number")
    private String serialNumber;

    @JsonProperty("model_id")
    private Long modelId;

    @JsonProperty("weight_limit")
    private Double weightLimit;

    @JsonProperty("battery_capacity")
    private Double batteryCapacity;

    @JsonProperty("state")
    private String state;

    public static Drone toDrone(DroneReq droneReq){
        Drone drone = new Drone();
        drone.setSerialNumber(droneReq.getSerialNumber());
        drone.setMedications(null);
        drone.setBatteryCapacity(droneReq.getBatteryCapacity());
        drone.setWeightLimit(droneReq.getWeightLimit());
        if(DroneServiceImp.droneStateMap.get(droneReq.getState().toUpperCase())==null){
            throw new InvalidDroneStateException();
        }
        drone.setState(DroneServiceImp.droneStateMap.get(droneReq.getState()));
        drone.setModel(null);
        return drone;
    }

}
