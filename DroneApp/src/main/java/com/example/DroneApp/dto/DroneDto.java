package com.example.DroneApp.dto;

import com.example.DroneApp.entity.Drone;
import com.example.DroneApp.entity.DroneModel;
import com.example.DroneApp.enums.StateType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Data
public class DroneDto {

    private Long id;

    private String serialNumber;

    private Long modelId;
    private String modelName;

    private Double weightLimit;

    private Double batteryCapacity;

    private String state;



//    public static Drone toDrone(DroneDto droneDto){
//        Drone drone = new Drone();
//        drone.setId(droneDto.getId());
//        drone.setSerialNumber(droneDto.getSerialNumber());
////        drone.setModel();
//        drone.set
//    }

    public DroneDto(DroneReq droneReq) {
        this.serialNumber = droneReq.getSerialNumber();
//        this.modelName = droneReq.getModelId();
        this.modelId = droneReq.getModelId();
        this.weightLimit = droneReq.getWeightLimit();
        this.batteryCapacity = droneReq.getBatteryCapacity();
        this.state = droneReq.getState();
    }
}
