package com.example.DroneApp.dto;

import com.example.DroneApp.entity.Drone;
import com.example.DroneApp.entity.Medication;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.*;

@Data
public class MedicationReq {

    private String name;

    private Double weight;

    private String code;

    private File image;

    @JsonProperty("drone_id")
    private Long droneId;

    public static Medication toMedication(String name, Double weight, String code, Long droneId, MultipartFile image) {
        Medication medication=new Medication();
        medication.setCode(code);
        medication.setName(name);
        medication.setWeight(weight);
        byte[] bFile = null;
        InputStream initialStream = null;
        try {
            initialStream = image.getInputStream();
            bFile = new byte[initialStream.available()];
            initialStream.read(bFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        medication.setImage(bFile);
        return medication;
    }
}
